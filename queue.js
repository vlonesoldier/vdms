class QueueObject{

    constructor(){
        this.elements = [];
    }

    add(elem, prior){
        if((prior < 0) || (prior >= 10)){
            throw 'Unsupported';        
        }

        this.elements.push([elem, prior])
        
    }

    delete(N){
        if(typeof this.elements[N] === 'undefined')
            throw 'Unsupported'

        if(this.elements.length === 0){
            console.log("No elements in queue");
            return -1;
        }
        else{
            this.elements.splice(N, 1);
        }
    }

    showAscending(){
        let temp;
        for(let i = 0; i < this.elements.length; i++){
            for(let j = 0; j < this.elements.length - i - 1; j++){
                if(this.elements[j][1] >= this.elements[j + 1][1]){
                  
                    temp = this.elements[j];
                    this.elements[j] = this.elements[j + 1];
                    this.elements[j + 1] = temp;
                }
            }
        }
        return this.elements;
    }

    showDescending(){
        let temp;
        for(let i = 0; i < this.elements.length; i++){
            for(let j = 0; j < this.elements.length - i - 1; j++){
                if(this.elements[j][1] <= this.elements[j + 1][1]){
                  
                    temp = this.elements[j];
                    this.elements[j] = this.elements[j + 1];
                    this.elements[j + 1] = temp;
                }
            }
        }
        return this.elements;
    }

    count(){
        return this.elements.length;
    }

    find(N){
        return this.elements[N][0];
    }

    isset(N){
        if(typeof this.elements[N] !== 'undefined')
            return 'ISTNIEJE'
        else
            return 'NIE ISTNIEJE'
    }
    

}

let queue = new QueueObject();
queue.add(1, 2);
queue.add(7, 1);
queue.add(8, 3);
queue.add(4, 0);

console.log(queue.find(0));
console.log(queue.count());

console.log(queue.showDescending())
console.log(queue.delete(1))
console.log(queue.showAscending())
console.log(queue.isset(4))
console.log(queue.isset(2))