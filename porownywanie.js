console.log(compare(process.argv[2], process.argv[3]))


function compare(one, two) {
    
    let [_one, _two] = [one, two];

    if (typeof one === 'undefined') 
        _one = '0';
    
    if (typeof two === 'undefined') 
        _two = '0';

    if( !(_one[_one.length - 1].includes('a') || _one[_one.length - 1].includes('b') || /\d/.test(_one[_one.length - 1])))
        return -1
    
   
    if( !(_two[_two.length - 1].includes('a') || _two[_two.length - 1].includes('b') || /\d/.test(_two[_two.length - 1])))
        return -1;


    for(let i = 0, j = 0; i < _one.length - 2, j < _two.length - 2; i++, j++){
        if((i % 2 == 0) && (/\d/.test(_one[i]) && (_one[i + 1] != '.')))
            return -1;
        
        if((j % 2 == 0) && (/\d/.test(_two[j]) && (_two[j + 1] != '.')))
            return -1;
   
    }

    

    var length;
    length = _one.length > _two.length ? _one.length : _two.length

    if ((_one.length > 7) || (_two.length > 7)) 
        return -1

        for (let i = 0; i < length; i++) {
            if ((_one[i] != '.') && (_two[i] != '.')) {
                if ((/\d/.test(_one[i]) && /\d/.test(_two[i]) && (Number(_one[i]) < Number(_two[i]))) || (!/\d/.test(_one[i]) && !/\d/.test(_two[i]) && (_one.charCodeAt(i) < _two.charCodeAt(i))) || ((typeof _two[i] == 'undefined') && (typeof _one[i] != 'undefined'))) {
                    return _two
                }
                else if ((/\d/.test(_one[i]) && /\d/.test(_two[i]) && (Number(_one[i]) > Number(_two[i]))) || (!/\d/.test(_one[i]) && !/\d/.test(_two[i]) && (_one.charCodeAt(i) > _two.charCodeAt(i)))|| ((typeof _two[i] != 'undefined') && (typeof _one[i] == 'undefined')))
                    return _one
                else if (i == length - 1)
                    return [_one, _two]
                continue;
            }
        }

    

}
