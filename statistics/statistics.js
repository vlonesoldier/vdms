const { readFileSync } = require('fs');

function readFile(file){
    if(!file)
        return -1;

    const text = readFileSync(file, 'utf-8')

    const lines = text.split(/\n/);

    return lines;
}

const lines = readFile('random.access.log')
let countCodes = [];
let uniqueIP = [];
let mainEntries = [];
let addressNoExist = []

for(let i in lines){
    if(lines[i].match(/HTTP\/1\.1"\s\d{3}/)){
        countCodes.push(lines[i].match(/HTTP\/1\.1"\s\d{3}/)[0])
    }

    if(lines[i].match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/))
        if(lines[i].match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)[0] && !uniqueIP.includes(lines[i].match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)[0]))
            uniqueIP.push(lines[i].match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)[0])

    if(lines[i].match(/[a-zA-Z]{3,}\s\/\sHTTP\/1.1\"\s\d{3}\s\d*\s\"-\"/))
        mainEntries.push(lines[i].match(/[a-zA-Z]{3,}\s\/\sH/))
    
    if(lines[i].match(/HTTP\/1\.1"\s404/) && !addressNoExist.includes(lines[i].match(/[a-zA-Z]{3,}\s\/(.)*\sHTTP/)[0].split(" ")[1])){
        addressNoExist.push(lines[i].match(/[a-zA-Z]{3,}\s\/(.)*\sHTTP/)[0].split(" ")[1])
    }
}

console.log('ILOSC WYSTAPIEN STATUS CODE: ' + countCodes.length);
console.log('ILOSC WYSTAPIEN UNIKALNYCH IP: ' + uniqueIP.length);
console.log(uniqueIP);
console.log('ILOSC WYSTAPIEN BEZPOSREDNICH WEJSC NA STRONE GLOWNA: ' + mainEntries.length);
console.log(addressNoExist);

